<?php
    App::uses('Controller', 'Controller');


    class UsersController extends Controller
    {
        var $name = 'Users';
        var $components = array('Auth');

        function login(){
            if ($this->Auth->login()) {
                $this->redirect($this->Auth->redirect());
            }else{
                $this->Session->setFlash(__('Username or password is incorrect'), 'default', array(), 'auth');
            }
        }
        function logout(){
            $this->redirect($this->Auth->logout());
        }
}


